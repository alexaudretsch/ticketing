﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ticketing.Models
{
    public class Seating
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Event")]
        [StringLength(50)]
        public string Event { get; set; }

        [DataType(DataType.Text)]
        [DisplayName("Venue")]
        [StringLength(50)]
        public string Venue { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Row")]
        [StringLength(4)]
        public string Row{ get; set;}

        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Seat Number")]
        [StringLength(3)]
        public int Seat { get; set; }
        
        [Required]
        [DataType(DataType.Text)]
        [DisplayName("Ticket Holder")]
        [StringLength(50)]
        public string Owner { get; set; }
    }
}
