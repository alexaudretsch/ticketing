﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Ticketing.Models
{
    public class Event
    {
            [Key]
            public int Id { get; set; }

        //TODO: Change to VenueID in data model
        //TODO: Keep as Venue is ViewModel
            [Required]
            [DisplayName("Venue")]      
            public int Venue { get; set; }

            [DataType(DataType.Text)]
            [DisplayName("Event Description")]
            [StringLength(500)]
            public string Description { get; set; }

            [DataType(DataType.Text)]
            [DisplayName("Event Summary")]
            [StringLength(250)]
            public string Summary { get; set; }
       
           [DataType(DataType.PhoneNumber)]
            [DisplayName("Event Phone Number")]
         [Phone()]
        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public long Phone { get; set; }
       
            [DataType(DataType.Duration)]
            [DisplayName("Event Duration")]
            public int Duration { get; set; }

            [DataType(DataType.Time)]
            [DisplayName("Event Start Time")]
            public DateTime StartTime { get; set; }

            [DataType(DataType.Time)]
            [DisplayName("Approximate End Time")]
            public DateTime EndTime { get; set; }

        //TODO: OwnerId (Created By Id)
            [DataType(DataType.Text)]
            public string Owner { get; set; }

            [DisplayName("Family Friendly Event")]
            public bool FamilyFriendly { get; set; }

            [DisplayName("All Ages Event")]
            public bool AllAges { get; set; }

            [DisplayName("18 and over event")]
            public bool EighteenAndOver { get; set; }

            [DisplayName("21 and over event")]
            public bool TwentyOneAndOver { get; set; }
       
            [DisplayName("Accomodations for Hearing Impaired")]
            public bool HearingImpaired { get; set; }

            [DisplayName("Wheelchair accessible")]
            public bool HandicapAccess { get; set; }

            [DisplayName("Allows Camping")]
            public bool AllowsCamping { get; set; }
        }
}