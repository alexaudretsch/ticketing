﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using static Ticketing.Models.Structs;

namespace Ticketing.Models
{
    public class Venue
    {
        public int Id { get; set; }

        [Required]
        public string Title { get; set; }

       [Required]
        public string Description { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:###-###-####}")]
        public long PhoneNumber { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [ScaffoldColumn(false)]
        public List<byte[]> Images { get; set; }

        [ScaffoldColumn(false)]
        public byte[] ThumbnailImage { get; set; }

        [Required]
        public string Owner { get; set; }
             
        
        [Required]
        [Display(Name = "Address Line 1")]
        public string AddressLine1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string AddressLine2 { get; set; }

        [Display(Name = "City")]
        [Required]
        [DataType(DataType.Text)]
        public string City { get; set; }

        [Required]
        [Display(Name = "State")]
        public UsState State { get; set; }

        [Required]
        [DataType(DataType.PostalCode)]
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

    }
}
