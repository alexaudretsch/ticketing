﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ticketing.Web.Startup))]
namespace Ticketing.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
