﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ticketing.Models;

namespace DataAccess
{
    public class SqlDatabase : IdentityDbContext<ApplicationUser>
    {
        public SqlDatabase()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static SqlDatabase Create()
        {
            return new SqlDatabase();
        }

        public DbSet<Event> Events { get; set; }

        public DbSet<Venue> Venues { get; set; }
       //  public DbSet<string> SomeStuff { get; set; }
    }
}
