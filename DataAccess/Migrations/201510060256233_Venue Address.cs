namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VenueAddress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Venues", "AddressLine1", c => c.String(nullable: false));
            AddColumn("dbo.Venues", "AddressLine2", c => c.String());
            AddColumn("dbo.Venues", "City", c => c.String(nullable: false));
            AddColumn("dbo.Venues", "State", c => c.Int(nullable: false));
            AddColumn("dbo.Venues", "ZipCode", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Venues", "ZipCode");
            DropColumn("dbo.Venues", "State");
            DropColumn("dbo.Venues", "City");
            DropColumn("dbo.Venues", "AddressLine2");
            DropColumn("dbo.Venues", "AddressLine1");
        }
    }
}
