namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class k : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Venues", "Location");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Venues", "Location", c => c.String(nullable: false));
        }
    }
}
