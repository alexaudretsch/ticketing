namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatingDatabase : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "AllowsCamping", c => c.Boolean(nullable: false));
            AlterColumn("dbo.Events", "Description", c => c.String(maxLength: 500));
            AlterColumn("dbo.Events", "Summary", c => c.String(maxLength: 250));
            DropColumn("dbo.Events", "Venue");
            DropColumn("dbo.Events", "Camping");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Events", "Camping", c => c.Boolean(nullable: false));
            AddColumn("dbo.Events", "Venue", c => c.String(nullable: false, maxLength: 50));
            AlterColumn("dbo.Events", "Summary", c => c.String(maxLength: 50));
            AlterColumn("dbo.Events", "Description", c => c.String(maxLength: 50));
            DropColumn("dbo.Events", "AllowsCamping");
        }
    }
}
