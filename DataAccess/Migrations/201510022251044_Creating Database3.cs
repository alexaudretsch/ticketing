namespace DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreatingDatabase3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Events", "Venue", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Events", "Venue");
        }
    }
}
