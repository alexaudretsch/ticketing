﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ticketing.Models;
using DataAccess;

namespace Ticketing.Services
{
   public  class VenueService
    {

        /// <summary>
        /// Inputs Venues into the database for testing.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public bool Create(Venue model)
        {

            using (var context = new DataAccess.SqlDatabase())  
            {
                var venue = new Venue();
                venue.Description = model.Description;
                venue.Email = model.Email;
                venue.Images = model.Images;
                venue.AddressLine1 = model.AddressLine1;
                venue.AddressLine2 = model.AddressLine2;
                venue.City = model.City;
                venue.State = model.State;
                venue.ZipCode = model.ZipCode;
                venue.Owner = model.Owner;
                venue.PhoneNumber = model.PhoneNumber;
                venue.ThumbnailImage = model.ThumbnailImage;
                venue.Title = model.Title;
                context.Venues.Add(venue);
                var result = context.SaveChanges();
                return result == -1; ;
            }


        }

    }
}
