﻿using DataAccess;
using System;
using Ticketing.Models;

namespace Ticketing.Services
{
    public class EventService
    {
        // TODO  Create an Create method that accepts an Event Edit View Model once the view model exists.

        /// <summary>
        /// Inputs Events into the database for testing purposes
        /// </summary>
        /// <param name="model"></param>
        /// <param name="userID"></param>
        /// <returns>bool success</returns
        public bool CreateTestData(Event model, Guid userID)
        {
            using (var context = new SqlDatabase())
            {
                var newEvent = new Event();
                newEvent.Description = model.Description;
                newEvent.AllAges = model.AllAges;
                newEvent.AllowsCamping = model.AllowsCamping;
                newEvent.Description = model.Description;
                newEvent.Duration = model.Duration;
                newEvent.EighteenAndOver = model.EighteenAndOver;
                newEvent.EndTime = model.EndTime;
                newEvent.FamilyFriendly = model.FamilyFriendly;
                newEvent.HandicapAccess = model.HandicapAccess;
                newEvent.HearingImpaired = model.HearingImpaired;
                newEvent.Owner = model.Owner;
                newEvent.Phone = model.Phone;
                newEvent.StartTime = model.StartTime;
                newEvent.Summary = model.Summary;
                newEvent.TwentyOneAndOver = model.TwentyOneAndOver;
                newEvent.Venue = model.Venue;
                context.Events.Add(newEvent);
                var result = context.SaveChanges();
                return result == -1;
            }
        }
    }
}